#bin/bash
cd /var/www/myhordes || exit
status=0
while [ $status -eq 0 ];
do
  php bin/console messenger:consume async -vv --time-limit=30
  status=$?
done